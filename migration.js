const Hotel = require('./models/hotel');
const fs = require('fs');

fs.readFile('data/data.json', 'utf8', async (err, data) => {
	try {
		const json = JSON.parse(data);

		await Hotel.insertMany(json);

		console.log("Se ha realizado la migración con éxito.");

		process.exit();
	}
	catch(err) {
		throw err;
	}
});