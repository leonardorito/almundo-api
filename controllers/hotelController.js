const Hotel = require('../models/hotel');

module.exports["getHotels"] = async (req,res) => {
  try {
    const hotels = await Hotel.find();
    const data = {
      hotels
    }
    res.json({
      status: 1,
      data
    })
  }
  catch(err) {
    res.json({
      status: 0,
      message: "Ha ocurrido un error"
    })
  }
}

module.exports["getFilterHotels"] = async (req,res) => {
  try {
    let fields = {};

    if (req.query.name) {
      fields.name = { $regex: req.query.name, $options: 'i' }
    }

    if (req.query.stars && req.query.stars.indexOf("all") < 0) {
      fields.stars = {$in: JSON.parse(req.query.stars)}
    }

    console.log(fields);

    const hotels = await Hotel.find(fields);
    const data = {
      hotels
    }
    res.json({
      status: 1,
      data
    })
  }
  catch(err) {
    console.log(err);
    res.json({
      status: 0,
      message: "Ha ocurrido un error"
    })
  }
}

module.exports["createHotel"] = async (req,res) => {
  try {
    let fields = {
      name: req.body.name,
      stars: req.body.stars,
      price: req.body.price,
      image: req.body.image,
      amenities: req.body.amenities
    };

    const HotelPre = new Hotel(fields);
    const hotel = await HotelPre.save();
    const data = {
      hotel
    }
    res.json({
      status: 1,
      message: "El hotel se ha guardado",
      data
    })
  }
  catch(err) {
    res.json({
      status: 0,
      message: "Ha ocurrido un error"
    })
  }
}

module.exports["updateHotel"] = async (req,res) => {
  try {
    let fields = {};

    if (req.body.name) {
      fields.name = req.body.name;
    }

    if (req.body.stars) {
      fields.stars = req.body.stars;
    }

    if (req.body.price) {
      fields.price = req.body.price;
    }

    if (req.body.image) {
      fields.image = req.body.image;
    }

    if (req.body.amenities) {
      fields.amenities = req.body.amenities;
    }

    const hotel = await Hotel.findByIdAndUpdate(req.body.hotel, fields, {new: true});

    const data = {
      hotel
    }
    res.json({
      status: 1,
      message: "El hotel se ha actualizado",
      data
    })
  }
  catch(err) {
    res.json({
      status: 0,
      message: "Ha ocurrido un error"
    })
  }
}

module.exports["removeHotel"] = async (req,res) => {
  try {
    const hotel = await Hotel.findByIdAndRemove(req.body.hotel);

    res.json({
      status: 1,
      message: "El hotel ha sido borrado"
    })
  }
  catch(err) {
    res.json({
      status: 0,
      message: "Ha ocurrido un error"
    })
  }
}
