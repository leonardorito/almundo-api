# Almundo API
API para ser usada en el frontend de Almundo

## Instrucciones para la instalación

NOTA: Para correr la API es recomendable tener instalado Node v8 o mayor, npm 6.4.1 o mayor y mongo 4.0

1. Crear una base de datos con un usuario con permisos ReadWrite. Se recomienda que tanto el nombre de usuario, como la contraseña, y el nombre de la base de datos sean almundo, y correr en el puerto 27017. Sin embargo, en caso de que no pueda ser asi, puede cambiar rápidamente en el archivo .env los datos de conexión a Mongo.
2. Dirigirse por terminal a la carpeta donde esté ubicado el proyecto y correr npm install.
3. Correr node migration.js. Este archivo se encargará de llenar la base de datos con los datos que fueron provistos en el data.json. En caso de que la migración haya sido exitosa, verá en consola el mensaje "Se ha realizado la migración con éxito.", en caso contrario, se mostrará el mensaje de error.
4. Finalmente, correr node app.js. Para este paso, se debe tener el puerto 3000 desocupado, en otro caso el servidor no podrá iniciar.