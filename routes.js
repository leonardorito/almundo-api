	var express = require('express');
	const router = express.Router();
	
	//Import Controllers	
	var hotelCtrl = require('./controllers/hotelController');

	
	router.route('/gethotels').get(hotelCtrl.getHotels);
	router.route('/getfilterhotels').get(hotelCtrl.getFilterHotels);
	router.route('/createhotel').post(hotelCtrl.createHotel);
	router.route('/updatehotel').put(hotelCtrl.updateHotel);
	router.route('/removehotel').delete(hotelCtrl.removeHotel);

	module.exports = router;	
