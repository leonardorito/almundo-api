'use strict';

const mongoose = require('../config/mongoose');

const modelName = 'Hotel';

const _ = require('lodash');

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  stars: {
    type: Number,
    required: true
  },
  price: {
    type: Number,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  amenities: [String]
}, {
  timestamps: {
    createdAt: 'created_at'
  }
});

module.exports = mongoose.model(modelName, schema);
